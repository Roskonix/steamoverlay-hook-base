#pragma once

#ifndef __CPEN_HEADER__
#define __CPEN_HEADER__

#include "..\Includes.h"

#define PI 3.14159265

//===================================================================================

class CPen
{
public:

	int iWidth, iHeight;

	virtual void	Init( void* Pram ) = 0;
	virtual void	Reset() = 0;
	virtual void	Text( float x, float y, DWORD dwColor, char* szText) = 0;
	virtual int		TextWidth( char* szText ) = 0;
	virtual int		TextHeight( char* szText ) = 0;
	virtual void	Line( float x1, float y1, float x2, float y2, DWORD dwColor ) = 0;
	virtual void	FillRGB( float x, float y, float w, float h, DWORD dwColor ) = 0;
	virtual void    FillRGBA( float x, float y, float w, float h, DWORD dwColor ) = 0;


	void			Fill( float x, float y, float w, float h, DWORD dwColor );
	void			FillCenter( float x, float y, float w, float h, DWORD dwColor );
	void			Textf( float x, float y, DWORD dwColor, const char* szText, ...);
	void			TextCenteredf( float x, float y, DWORD dwColor, const char* szText, ...);
	void			Circle( float x, float y, float r, float s, DWORD dwColor );
	void			Box( float x, float y, float w, float h, DWORD dwColor );
	void			BoxCentered( float x, float y, float w, float h, DWORD dwColor );
};

//===================================================================================

class CDX9Pen : public CPen
{
private:
	LPD3DXFONT			pFont;
	LPDIRECT3DDEVICE9	pDevice;
	LPD3DXLINE			pLine;

	struct Vertex_t
	{
		XMFLOAT4 xyzrhw;
		DWORD color;

		enum
		{
			FVF = D3DFVF_XYZRHW | D3DFVF_DIFFUSE
		};

	} Vertex, *pVertex;

	void BuildVertex( XMFLOAT4 xyzrhw, D3DCOLOR color, Vertex_t* vertexList, int index )
	{
		vertexList[ index ].xyzrhw = xyzrhw;
		vertexList[ index ].color = color;
	}

public:
	CDX9Pen()
	{
		pDevice = NULL;
		pFont	= NULL;
		pLine	= NULL;
	}

	virtual void Init( void* Pram )
	{
		if( !pDevice )
			pDevice = ( LPDIRECT3DDEVICE9 )Pram;

		if( !pFont )
			if( D3DXCreateFont( pDevice, 50, 0, FW_NORMAL, 1, FALSE, DEFAULT_CHARSET,
				OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Impact", &pFont ) != D3D_OK ) return;
	}

	virtual void Reset()
	{
		if( pFont )
		{
			pFont->Release();
			pFont = NULL;
		}
	}

	virtual void Text( float x, float y, DWORD dwColor, char* szText)
	{
		RECT pos = { ( long )x, ( long )y, ( long )0, ( long )0 };
		pFont->DrawTextA( NULL, szText, -1, &pos, DT_NOCLIP, dwColor );
	}

	virtual int TextWidth( char* szText )
	{
		RECT pos = { 0, 0, 0, 0 };
		pFont->DrawTextA( NULL, szText, -1, &pos, DT_CALCRECT, 0 );
		return pos.right - pos.left;
	}

	virtual int TextHeight( char* szText )
	{
		RECT pos = { 0, 0, 0, 0 };
		pFont->DrawTextA( NULL, szText, -1, &pos, DT_CALCRECT, 0 );
		return pos.bottom - pos.top;
	}

	virtual void Line( float x1, float y1, float x2, float y2, DWORD dwColor )
	{
		D3DXCreateLine( pDevice, &pLine );
		D3DXVECTOR2 vLines[] = { D3DXVECTOR2( x1, y1 ), D3DXVECTOR2( x2, y2 ) };
		pLine->Begin();
		pLine->Draw( vLines, 2, dwColor );
		pLine->End();
		pLine->Release();
	}


	virtual void FillRGB( float x1, float y1, float x2, float y2, DWORD dwColor )
	{
		D3DRECT rec = { ( long )x1, ( long )y1, ( long )x2, ( long )y2 };
		pDevice->Clear( 1, &rec, D3DCLEAR_TARGET, dwColor, 0, 0 );
	}

	virtual void FillRGBA( float x, float y, float w, float h, DWORD dwColor )
	{
		Vertex_t vertexList[ 4 ];

		BuildVertex( XMFLOAT4(     x, y + h, 0, 1 ), dwColor, vertexList, 0 );
		BuildVertex( XMFLOAT4(     x,     y, 0, 1 ), dwColor, vertexList, 1 );
		BuildVertex( XMFLOAT4( x + w, y + h, 0, 1 ), dwColor, vertexList, 2 );
		BuildVertex( XMFLOAT4( x + w,     y, 0, 1 ), dwColor, vertexList, 3 );

		pDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, vertexList, sizeof( Vertex_t ) );
	}

};

//===================================================================================

extern CDX9Pen cDX9Pen;
extern CPen* cPen;

//===================================================================================

#endif