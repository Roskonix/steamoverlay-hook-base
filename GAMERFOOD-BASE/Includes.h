#pragma once

#ifndef __INCLUDES_HEADER__
#define __INCLUDES_HEADER__

#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS


#define _BASE_LOG "GAMERFOOD.log"

//===================================================================================
// Log mode: LOG_BOTH - 3
//===================================================================================
#define LOG_MODE 3

#include <d3d9.h>
#include <d3dx9.h>
#include <DirectXMath.h>
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

using namespace DirectX;

//===================================================================================

#include <windows.h>
#include <string>
#include <math.h>
#include <vector>
#include <sstream>
#include <memory>

//===================================================================================

#include "Utils\CThread.h"
#include "Utils\CSig.h"
#include "Utils\CLog.h"

#include "Hooks\Overlay.h"

#include "Drawing\CPen.h"




#endif
