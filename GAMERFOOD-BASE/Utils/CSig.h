#pragma once

#ifndef __CSIG_HEADER__
#define __CSIG_HEADER__

#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <vector>
#include "..\Includes.h"

using namespace std;

//===================================================================================

typedef struct _FSig 
{
	DWORD ulAddress;
	char szName[ 256 ];

	void SetAddress( DWORD ulNewAddress )
	{ 
		ulAddress = ulNewAddress; 
	}

	DWORD GetAddress(){ return ulAddress; }

	char *GetName(){ return szName; }

} FSig;

//===================================================================================

class CSig
{
public:
					CSig();
	void			Clear();
	bool			SetRange( DWORD ulMin, DWORD ulMax );
	bool			SetRange( HMODULE hModule );
	FSig*			ManualAdd( const char* pszName, DWORD ulAddressOf );
	FSig*			AddSig( const char* pszName, BYTE* pbMask, CHAR* pszMask, DWORD offset );
	FSig*			GetSigByName( const char* pszName );
	FSig*			GetSigByIndex( int iIndex );
	int				GetSigCount();
	vector< FSig >	GetVector();
	DWORD	        GetRangeMin();
	DWORD	        GetRangeMax();

private:

	bool			DataComp( BYTE* pbData, BYTE* pbMask, const char* pszMask );
	DWORD	        DetectPattern( BYTE* pbMask, const char* pszMask );

	DWORD	        ulRangeMin;
	DWORD	        ulRangeMax;
	vector< FSig >	vSigs;
};

//===================================================================================

extern CSig cSig;

//===================================================================================

#endif