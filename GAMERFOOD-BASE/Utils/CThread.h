#pragma once

#ifndef __CTHREAD_HEADER__
#define __CTHREAD_HEADER__

#include <windows.h>
#include "ntdll.h"
#include "..\Includes.h"


class CThread
{
public:

	CThread( HANDLE hThread )
	{
		Thread = hThread;
	}
	
	static CThread New( LPTHREAD_START_ROUTINE AddressOfRoutine, void* params = NULL )
	{
		return CThread( CreateThread( 0, 0, AddressOfRoutine, params, 0, 0 ) );
	}
	
	static CThread NewAsync( LPTHREAD_START_ROUTINE AddressOfRoutine, void *Params = NULL )
	{
		HANDLE hThread = CreateThread( 0, 0, AddressOfRoutine, Params, 0, 0 );

		WaitForSingleObject( hThread, INFINITE );

		return CThread( hThread );
	}

	static CThread NewRemote( HANDLE Process, LPTHREAD_START_ROUTINE AddressOfRoutine, void* params = NULL )
	{
		NtCreateThreadExBuffer ntBuffer;
		DWORD dw0 = 0;
		DWORD dw1 = 0;
		memset( &ntBuffer, 0, sizeof( NtCreateThreadExBuffer ) );

		ntBuffer.Size = sizeof( NtCreateThreadExBuffer );
		ntBuffer.Unknown1 = 0x10003;
		ntBuffer.Unknown2 = 0x8;
		ntBuffer.Unknown3 = &dw1;
		ntBuffer.Unknown4 = 0;
		ntBuffer.Unknown5 = 0x10004;
		ntBuffer.Unknown6 = 4;
		ntBuffer.Unknown7 = &dw0;

		LP_NtCreateThreadEx* _NtCreateThreadEx = ( LP_NtCreateThreadEx* )GetProcAddress( GetModuleHandle( "ntdll.dll" ), "NtCreateThreadEx" );

		if( _NtCreateThreadEx == NULL )
			return 0;

		HANDLE hThread = NULL;
		HRESULT hRes = 0;

		if( !SUCCEEDED( hRes = _NtCreateThreadEx( &hThread, 0x1FFFFF, NULL, Process, AddressOfRoutine, params, FALSE, 0, 0, 0, &ntBuffer ) ) )
			return 0;
	
		return CThread( hThread );
	}
	
	bool isValid()
	{
		return ( Thread != INVALID_HANDLE_VALUE );
	}
	
	bool Suspend() 
	{
		if( isValid() == false ) 
			return false;

		return ( SuspendThread( Thread ) != 0 );
	}
	
	bool Resume() 
	{
		if( isValid() == false ) 
			return false;

		return ( ResumeThread( Thread ) != 0 );
	}
	
	bool Destroy()
	{
		if( isValid() == false )
			return false;

		WaitForSingleObject(Thread, INFINITE);
		CloseHandle(Thread);
		return true;
	}



private:
	HANDLE Thread;

	typedef DWORD WINAPI LP_NtCreateThreadEx
		(
		PHANDLE hThread,
		ACCESS_MASK DesiredAccess,
		LPVOID ObjectAttributes,
		HANDLE ProcessHandle,
		LPTHREAD_START_ROUTINE lpStartAddress,
		LPVOID lpParameter,
		BOOL CreateSuspended,
		DWORD StackZeroBits,
		DWORD SizeOfStackCommit,
		DWORD SizeOfStackReserve,
		LPVOID lpBytesBuffer
		);

	struct NtCreateThreadExBuffer
	{
		ULONG Size;
		ULONG Unknown1;
		ULONG Unknown2;
		PULONG Unknown3;
		ULONG Unknown4;
		ULONG Unknown5;
		ULONG Unknown6;
		PULONG Unknown7;
		ULONG Unknown8;
	} UNKNOWN;

};

#endif
