#include "CLog.h"

CLog cLog;

void CLog::BuildDebugConsole( const char* szTitle )
{
	static bool bOnce = FALSE;
	if( !bOnce )
	{
		if( AllocConsole() )
		{
			hDebugConsole = GetStdHandle( STD_OUTPUT_HANDLE );
			SetConsoleTitle( szTitle );
			WriteConsole( hDebugConsole, "Don't be a noob get gamerfood!\n\nDebug console loaded\n\n",
				strlen( "Don't be a noob get gamerfood!\n\nDebug console loaded\n\n" ), &dwMessage, NULL );
			
			bOnce = TRUE;
		}
	}
}

void CLog::Log( const char * pszMessage, ... )
{
	va_list va_alist;
	char szLogbuf[ 4096 ];
	FILE * fp;
	struct tm * current_tm;
	time_t current_time;

	time( &current_time );
	current_tm = localtime( &current_time );

	sprintf( szLogbuf, "[%02d:%02d:%02d] ", current_tm->tm_hour, current_tm->tm_min, current_tm->tm_sec );

	va_start( va_alist, pszMessage );
	_vsnprintf( szLogbuf + strlen( szLogbuf ), sizeof( szLogbuf ) - strlen( szLogbuf ), pszMessage, va_alist );
	va_end( va_alist );
	sprintf(szLogbuf, "%s\n", szLogbuf);
	if( ( fp = fopen( GetDirectoryFile( ".log" ), "a" ) ) != NULL )
	{
		fprintf( fp, "%s", szLogbuf );
		fclose( fp );
	}

	::WriteConsole( hDebugConsole, szLogbuf, strlen( szLogbuf ), &dwMessage, NULL );

}
void CLog::LogBaseUponModule(HMODULE hmModule) {
	GetModuleFileName( hmModule, szDirectory, 512 );
	for( int i = ( int )strlen( szDirectory ); i > 0; i-- ) {
		if( szDirectory[ i ] == '\\' )
			szDirectory[ i + 1 ] = 0; break;
	}
}
char* CLog::GetDirectoryFile( char *szFile )
{
	static char path[ 320 ];
	strcpy( path, szDirectory );
	strcat( path, szFile );
	return path;
}

