#include "Includes.h"


DWORD WINAPI lpGetSigs( LPVOID lpParams )
{
	if( cSig.SetRange( GetModuleHandle( "gameoverlayrenderer.dll" ) ) )
	{
		cLog.Log( "[CSIG  ] Range Set:\t[0x%.8X, 0x%.8X]", cSig.GetRangeMin(), cSig.GetRangeMax() );

		FSig* Overlay_Pointer =
			cSig.AddSig( "Overlay_Pointer",
			( BYTE* )"\xFF\x15\x00\x00\x00\x00\x8B\xF8\x85\xDB\x74\x1F",
			( CHAR* )"xx????xxxxxx", 2 );

		if( Overlay_Pointer == NULL || Overlay_Pointer->GetAddress() == 0 )
			cLog.Log( "ERR:[CSIG  ] Failed to find Overlay_Pointer" );

		//Overlay_Pointer->SetAddress( *( DWORD* )( Overlay_Pointer->ulAddress + 2 ) );
	}

	for( int i = 0; i < static_cast< int >( cSig.GetSigCount() ); ++i )
	{
		FSig* pSig = cSig.GetSigByIndex( i );
		if( pSig == NULL ) continue;

		cLog.Log( "[CSIG  ] Sig\t[%s] =>\t[0x%.8X]", pSig->GetName(), pSig->GetAddress() );
	}
	return FALSE;
}

DWORD WINAPI lpHookInterns( LPVOID lpParams )
{
	FSig* pOverlayPointer = cSig.GetSigByName( "Overlay_Pointer" );

	if( pOverlayPointer )
	{
		DWORD_PTR* Present_V = ( DWORD_PTR* )( pOverlayPointer->GetAddress() );

		cLog.Log( "[HOOKS ] Present_V points to:\t[0x%.8X]", *Present_V );

		extern Present_T Present_O;
		extern HRESULT _stdcall Present_H( LPDIRECT3DDEVICE9 pDevice, RECT* pSourceRect, 
										   RECT* pDestRect, HWND hDestWindowOverride, RGNDATA* pDirtyRegion );
		
		cPen = &cDX9Pen; // load drawing platform


		Present_O = ( Present_T )( *Present_V );
		*Present_V = ( DWORD_PTR )( &Present_H );

		cLog.Log( "[HOOKS ] Present Hooked");
		cLog.Log( "[HOOKS ] All hooks in place capt'n!" );
		*( BOOL* )lpParams = TRUE; return 0;	
	}
	return 0;
}

DWORD WINAPI lpThreadController( LPVOID lpParams )
{
	CThread::NewAsync( lpGetSigs ).isValid() 
		? cLog.Log( "[THREAD] GetSigs thread launched successfully!" )
		: cLog.Log( "ERR:[THREAD] GetSigs thread failed to launch!!!" );

	BOOL bHooked = FALSE;
	
	while( !bHooked )
	{
		CThread::NewAsync( lpHookInterns, &bHooked ).isValid()
			? cLog.Log( "[THREAD] HookInterns thread launched successfully!" )
			: cLog.Log( "ERR:[THREAD] HookInterns thread failed to launch!!!" );

		Sleep( 500 );
	}
	

	return FALSE;
}

BOOL APIENTRY DllMain( HMODULE hModule, DWORD dwReason, LPVOID lpReserved )
{
	if( dwReason == DLL_PROCESS_ATTACH )
	{
		cLog.BuildDebugConsole( "Gamerfood Base Debug" );
		cLog.LogBaseUponModule( ( HMODULE )hModule );
		
		CThread::New( lpThreadController, hModule ).isValid()
			? cLog.Log( "[THREAD] ThreadController thread launched successfully!" )
			: cLog.Log( "ERR:[THREAD] ThreadController thread failed to launch!!!" );
	}

	return TRUE;
}
